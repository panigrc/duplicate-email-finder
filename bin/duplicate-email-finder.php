<?php

declare(strict_types=1);

use DuplicateEmailFinder\DuplicateEmailFinderCommand;
use Symfony\Component\Console\Application;

require_once __DIR__ . '/../vendor/autoload.php';

$application = new Application('duplicate-email-finder', '0.1.0');
$command = new DuplicateEmailFinderCommand();

$application->add($command);

$application->setDefaultCommand($command->getName(), true);
try {
	$application->run();
} catch (Exception $e) {
	echo "Something went wrong: {$e->getMessage()}\n";
	var_dump($e->getTrace());
	exit(1);
}
