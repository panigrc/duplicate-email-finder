<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use DateTime;

class Email
{
	/** @var EmailAttachment[] */
	protected $attachments;

	/** @var null|string */
	protected $htmlContent;

	/** @var null|string */
	protected $textContent;

	/** @var EmailAddress[] */
	protected $ccAddresses;

	/** @var EmailAddress[] */
	protected $toAddresses;

	/** @var DateTime */
	protected $date;

	/** @var null|string */
	protected $fromName;

	/** @var string */
	protected $fromEmail;

	/**
	 * @return EmailAttachment[]
	 */
	public function getAttachments(): array
	{
		return $this->attachments;
	}

	/**
	 * @param EmailAttachment[] $attachments
	 */
	public function setAttachments(array $attachments): void
	{
		$this->attachments = $attachments;
	}

	/**
	 * @return string|null
	 */
	public function getHtmlContent(): ?string
	{
		return $this->htmlContent;
	}

	/**
	 * @param string|null $htmlContent
	 */
	public function setHtmlContent(?string $htmlContent): void
	{
		$this->htmlContent = $htmlContent;
	}

	/**
	 * @return string|null
	 */
	public function getTextContent(): ?string
	{
		return $this->textContent;
	}

	/**
	 * @param string|null $textContent
	 */
	public function setTextContent(?string $textContent): void
	{
		$this->textContent = $textContent;
	}

	/**
	 * @return EmailAddress[]
	 */
	public function getCcAddresses(): array
	{
		return $this->ccAddresses;
	}

	/**
	 * @param EmailAddress[] $ccAddresses
	 */
	public function setCcAddresses(array $ccAddresses): void
	{
		$this->ccAddresses = $ccAddresses;
	}

	/**
	 * @return EmailAddress[]
	 */
	public function getToAddresses(): array
	{
		return $this->toAddresses;
	}

	/**
	 * @param EmailAddress[] $toAddresses
	 */
	public function setToAddresses(array $toAddresses): void
	{
		$this->toAddresses = $toAddresses;
	}

	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime
	{
		return $this->date;
	}

	/**
	 * @param DateTime $date
	 */
	public function setDate(DateTime $date): void
	{
		$this->date = $date;
	}

	/**
	 * @return string|null
	 */
	public function getFromName(): ?string
	{
		return $this->fromName;
	}

	/**
	 * @param string|null $fromName
	 */
	public function setFromName(?string $fromName): void
	{
		$this->fromName = $fromName;
	}

	/**
	 * @return string
	 */
	public function getFromEmail(): string
	{
		return $this->fromEmail;
	}

	/**
	 * @param string $fromEmail
	 */
	public function setFromEmail(string $fromEmail): void
	{
		$this->fromEmail = $fromEmail;
	}
}
