<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

class Maildir
{
	/** @var string[] Default Maildir Directory structure */
	public const DIR_STRUCTURE = ['/cur', '/new', '/tmp'];

	/** @var string */
	private $path;

	public function __construct(string $path)
	{
		$this->setPath(rtrim($path, '/'));
	}

	/**
	 * @return string
	 */
	public function getPath(): string
	{
		return $this->path;
	}

	/**
	 * @param string $path
	 */
	public function setPath(string $path): void
	{
		$this->path = $path;
	}

	public function getSubDirs(): array
	{
		return glob($this->getPath() . '/.[^.]*', GLOB_ONLYDIR);
	}

	public function getEmailPaths(): array
	{
		$emailPaths = [];
		$emailPaths[] = $this->getEmailPathsForDir($this->getPath());

		foreach ($this->getSubDirs() as $dir) {
			$emailPaths[] = $this->getEmailPathsForDir($dir);
		}

		return array_merge(... $emailPaths);
	}

	private function getEmailPathsForDir(string $path): array
	{
		$emailPaths = [];

		foreach (self::DIR_STRUCTURE as $dir) {
			$emailPaths[] = glob($path . $dir . '/*');
		}

		return array_merge(... $emailPaths);
	}
}
