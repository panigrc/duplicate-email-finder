<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

class EmailAddress
{
	/** @var string|null */
	protected $name;

	/** @var string */
	protected $email;

	/**
	 * @return string|null
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param string|null $name
	 */
	public function setName(?string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email): void
	{
		$this->email = $email;
	}
}
