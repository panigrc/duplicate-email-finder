<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use Exception;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use ZBateson\MailMimeParser\MailMimeParser;

class DuplicateEmailFinderCommand extends Command
{
	protected static $defaultName = 'duplicate-email-finder';

	protected function configure(): void
	{
		$this
			->setDescription('Find duplicate Emails in a Maildir directory.')
			->addArgument('maildir', InputArgument::REQUIRED, 'Maildir path')
			->addOption('no-cache', null, InputOption::VALUE_NONE, 'Disables the use of the cache and refreshes the cache')
			->addOption('files', null, InputOption::VALUE_NONE, 'Outputs only email file paths')
			->addOption('exclude-first', null, InputOption::VALUE_NONE, 'Exclude first duplicate email file path from output')
			->addOption('exclude-last', null, InputOption::VALUE_NONE, 'Exclude last duplicate email file path from output')
			->addOption('output', null, InputOption::VALUE_REQUIRED, 'Save output to a file instead of displaying it')
			->addOption('delete', null, InputOption::VALUE_NONE, 'Delete the selected emails')
			->addOption('use-attachment-content', null, InputOption::VALUE_NONE, 'Add E-mail Attachment Contents to the comparison criteria')
			->addOption('use-attachment-filename', null, InputOption::VALUE_NONE, 'Add E-mail Attachment Filenames to the comparison criteria')
			->addOption('use-attachment-mimetype', null, InputOption::VALUE_NONE, 'Add E-mail Attachment Mimetypes to the comparison criteria')
			->addOption('use-body-html', null, InputOption::VALUE_NONE, 'Add E-mail Body HTML to the comparison criteria')
			->addOption('use-body-text', null, InputOption::VALUE_NONE, 'Add E-mail Body Text to the comparison criteria')
			->addOption('use-cc-email', null, InputOption::VALUE_NONE, 'Add E-mail CC Addresses to the comparison criteria')
			->addOption('use-cc-name', null, InputOption::VALUE_NONE, 'Add E-mail CC Names to the comparison criteria')
			->addOption('use-date', null, InputOption::VALUE_NONE, 'Add E-mail Date to the comparison criteria')
			->addOption('use-from-email', null, InputOption::VALUE_NONE, 'Add E-mail From Addresses to the comparison criteria')
			->addOption('use-from-name', null, InputOption::VALUE_NONE, 'Add E-mail From Names to the comparison criteria')
			->addOption('use-to-email', null, InputOption::VALUE_NONE, 'Add E-mail to-address to the comparison criteria')
			->addOption('use-to-name', null, InputOption::VALUE_NONE, 'Add E-mail to-name to the comparison criteria');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$maildir = new Maildir($input->getArgument('maildir'));

		$maildirValidator = new MaildirValidator($maildir);

		try {
			$maildirValidator->validate();

			$emailHashGenerator = new EmailHashGenerator($this->createBitmask($input));

			$duplicateEmailProvider = new DuplicateEmailProvider(
				$maildir,
				new EmailHashCache('/tmp/emailHashCache.php'),
				$emailHashGenerator,
				new MailMimeParser()
			);

			if ($input->getOption('files') || $input->getOption('delete')) {
				$duplicateEmailProvider->onlyFiles();
			}
			if ($input->getOption('exclude-first')) {
				$duplicateEmailProvider->excludeFirst();
			}
			if ($input->getOption('exclude-last')) {
				$duplicateEmailProvider->excludeLast();
			}

			if (($outputFile = $input->getOption('output')) && !touch($outputFile)) {
				$output->writeln("<error>Could not write file {$outputFile}</error>");

				return Command::FAILURE;
			}

			$result = $duplicateEmailProvider->get(! $input->getOption('no-cache'));


			if ($input->getOption('delete')) {

				$outputStyle = new OutputFormatterStyle('yellow', null, ['bold', 'blink']);
				$output->getFormatter()->setStyle('yellowbb', $outputStyle);
				$outputStyle = new OutputFormatterStyle('yellow', null, ['bold']);
				$output->getFormatter()->setStyle('yellow', $outputStyle);

				if (! $input->getOption('exclude-first') && ! $input->getOption('exclude-last')) {
					$output->write('<yellowbb>Warning!</yellowbb>');
					$output->writeln(
						"<yellow> You didn't use either 'exclude-first' or 'exclude-last' option,"
						. " which means that ALL of the duplicate emails will be deleted</yellow>"
					);
				}

				$helper = $this->getHelper('question');
				$question = new ConfirmationQuestion('Are you sure you want do delete ' . count($result) . ' emails ?', false);

				if (! $helper->ask($input, $output, $question)) {
					$output->writeln('Process cancelled');

					return Command::SUCCESS;
				}

				$question = new ConfirmationQuestion('Let me ask you again, do you want to continue?', false);

				if (! $helper->ask($input, $output, $question)) {
					$output->writeln('Process cancelled');

					return Command::SUCCESS;
				}

				foreach($result as $file) {
					if (false === unlink($file)) {
						$output->writeln("<error>Could not remove file {$file}</error>");
					}
				}
				$output->writeln('<info>Successfully deleted ' . count($result) . ' emails</info>');

				return Command::SUCCESS;
			}

			if (! $input->getOption('files')) {
				$result = var_export($result, true);
			}

			if (isset($outputFile)) {
				file_put_contents($outputFile, $result);
			} else {
				$output->writeln($result);
			}


		} catch (RuntimeException | Exception $e) {
			$output->writeln("<error>{$e->getMessage()}</error>");

			return Command::FAILURE;
		}

		return Command::SUCCESS;
	}

	private function createBitmask(InputInterface $input): int
	{
		$bitmask = null;

		if ($input->getOption('use-attachment-content')) {
			$bitmask |= EmailHashGenerator::USE_ATTACHMENT_CONTENT;
		}
		if ($input->getOption('use-attachment-filename')) {
			$bitmask |= EmailHashGenerator::USE_ATTACHMENT_FILENAME;
		}
		if ($input->getOption('use-attachment-mimetype')) {
			$bitmask |= EmailHashGenerator::USE_ATTACHMENT_MIMETYPE;
		}
		if ($input->getOption('use-body-html')) {
			$bitmask |= EmailHashGenerator::USE_BODY_HTML;
		}
		if ($input->getOption('use-body-text')) {
			$bitmask |= EmailHashGenerator::USE_BODY_TEXT;
		}
		if ($input->getOption('use-cc-email')) {
			$bitmask |= EmailHashGenerator::USE_CC_EMAIL;
		}
		if ($input->getOption('use-cc-name')) {
			$bitmask |= EmailHashGenerator::USE_CC_NAME;
		}
		if ($input->getOption('use-date')) {
			$bitmask |= EmailHashGenerator::USE_DATE;
		}
		if ($input->getOption('use-from-email')) {
			$bitmask |= EmailHashGenerator::USE_FROM_EMAIL;
		}
		if ($input->getOption('use-from-name')) {
			$bitmask |= EmailHashGenerator::USE_FROM_NAME;
		}
		if ($input->getOption('use-to-email')) {
			$bitmask |= EmailHashGenerator::USE_TO_EMAIL;
		}
		if ($input->getOption('use-to-name')) {
			$bitmask |= EmailHashGenerator::USE_TO_NAME;
		}

		return $bitmask ?:
			EmailHashGenerator::USE_BODY_HTML
			| EmailHashGenerator::USE_BODY_TEXT
			| EmailHashGenerator::USE_CC_EMAIL
			| EmailHashGenerator::USE_CC_NAME
			| EmailHashGenerator::USE_DATE
			| EmailHashGenerator::USE_FROM_EMAIL
			| EmailHashGenerator::USE_FROM_NAME
			| EmailHashGenerator::USE_TO_EMAIL
			| EmailHashGenerator::USE_TO_NAME;
	}
}
