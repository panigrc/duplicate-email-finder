<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use RuntimeException;
use Throwable;

class MaildirValidationException extends RuntimeException
{
	public const MAILDIR_DOES_NOT_EXIST = 1;

	public const MAILDIR_HAS_NOT_VALID_FORMAT = 2;

	public function __construct($message = '', $code = 0, Throwable $previous = null)
	{
		switch ($code) {
			case self::MAILDIR_DOES_NOT_EXIST:
				$message = 'Maildir does not exist. ' . $message;
				break;
			case self::MAILDIR_HAS_NOT_VALID_FORMAT:
				$message = 'Maildir has not a valid format. ' . $message;
				break;
		}

		parent::__construct(trim($message), $code, $previous);
	}
}
