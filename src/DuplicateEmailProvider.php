<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use RuntimeException;
use ZBateson\MailMimeParser\MailMimeParser;

class DuplicateEmailProvider
{
	/** @var Maildir */
	private $maildir;

	/** @var EmailHashCache */
	private $emailHashCache;

	/** @var EmailHashGenerator */
	private $emailHashGenerator;

	/** @var MailMimeParser */
	private $mailMimeParser;

	/** @var bool */
	private $onlyFiles = false;

	/** @var bool */
	private $excludeFirst = false;

	/** @var bool */
	private $excludeLast = false;

	public function __construct(
		Maildir $maildir,
		EmailHashCache $emailHashCache,
		EmailHashGenerator $emailHashGenerator,
		MailMimeParser $mailMimeParser
	)
	{
		$this->maildir = $maildir;
		$this->emailHashCache = $emailHashCache;
		$this->emailHashGenerator = $emailHashGenerator;
		$this->mailMimeParser = $mailMimeParser;
	}

	/**
	 * When set, the get() method returns only
	 * an array of email file paths.
	 *
	 * @return $this
	 */
	public function onlyFiles(): self
	{
		$this->onlyFiles = true;

		return $this;
	}

	/**
	 * When set, the get() method excludes the first
	 * email path from the duplicate emails.
	 *
	 * @return $this
	 */
	public function excludeFirst(): self
	{
		$this->excludeFirst = true;

		return $this;
	}

	/**
	 * When set, the get() method excludes the last
	 * email path from the duplicate emails.
	 *
	 * @return $this
	 */
	public function excludeLast(): self
	{
		$this->excludeLast = true;

		return $this;
	}

	/**
	 * @param bool $useCache
	 * @return array
	 */
	public function get(bool $useCache = true): array
	{
		if ($useCache && ! $this->emailHashCache->exists()) {
			$this->cacheEmails();
		}

		return $this->getDuplicateEmails();
	}

	private function cacheEmails(): void
	{
		foreach ($this->maildir->getEmailPaths() as $emailPath) {

			if (false === file_exists($emailPath) || false === $emailFile = file_get_contents($emailPath)) {
				throw new RuntimeException("Could not read email file in [{$emailPath}]");
			}

			$email = new EmailAdapter($this->mailMimeParser->parse($emailFile));

			$hash = $this->emailHashGenerator->generate($email);

			$this->emailHashCache->append($hash, $emailPath);
		}

		$this->emailHashCache->flush();
	}

	private function getDuplicateEmails(): array
	{
		$this->emailHashCache->read();

		$result = [];
		$files = [];
		foreach($this->emailHashCache->get() as $value) {
			$result[$value['hash']][] = $value['email_path'];
		}

		foreach($result as $hash => $emailPaths) {
			if (count($result[$hash]) === 1) {
				unset($result[$hash]);

				continue;
			}

			if ($this->excludeFirst) {
				array_shift($result[$hash]);
			}

			if ($this->excludeLast) {
				array_pop($result[$hash]);
			}

			$files[] = $result[$hash];
		}

		if ($this->onlyFiles) {
			return array_merge( ... $files);
		}
		return $result;
	}
}
