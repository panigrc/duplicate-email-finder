<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;


class EmailHashGenerator
{
	public const USE_ATTACHMENT_CONTENT = 1;
	public const USE_ATTACHMENT_FILENAME = 2;
	public const USE_ATTACHMENT_MIMETYPE = 4;
	public const USE_BODY_HTML = 8;
	public const USE_BODY_TEXT = 16;
	public const USE_CC_EMAIL = 32;
	public const USE_CC_NAME = 64;
	public const USE_DATE = 128;
	public const USE_FROM_EMAIL = 256;
	public const USE_FROM_NAME = 512;
	public const USE_TO_EMAIL = 1024;
	public const USE_TO_NAME = 2048;

	/**
	 * @var int Email components to use
	 *  in order to generate the hash.
	 *  Bitmask of
     *  USE_ATTACHMENT_CONTENT,
     *  USE_ATTACHMENT_FILENAME,
     *  USE_ATTACHMENT_MIMETYPE,
     *  USE_BODY_HTML,
     *  USE_BODY_TEXT,
     *  USE_CC_EMAIL,
     *  USE_CC_NAME,
     *  USE_DATE,
     *  USE_FROM_EMAIL,
     *  USE_FROM_NAME,
     *  USE_TO_EMAIL,
     *  USE_TO_NAME
	 */
	private $flags;

	public function __construct(int $flags)
	{
		$this->flags = $flags;
	}

	/**
	 * @param Email $email
	 * @param string $hashingAlgorithm See PHP hash() function
	 * @return string
	 */
	public function generate(Email $email, string $hashingAlgorithm = 'sha1'): string
	{
		$contentToHash = '';

		if ($this->flags & self::USE_ATTACHMENT_CONTENT) {
			$contentToHash .= array_reduce(
				$email->getAttachments(),
				static function ($carry, $item) {
					/** @var EmailAttachment $item */
					$carry .= $item->getContent();
					return $carry;
				},
			'');
		}

		if ($this->flags & self::USE_ATTACHMENT_FILENAME) {
			$contentToHash .= array_reduce(
				$email->getAttachments(),
				static function ($carry, $item) {
					/** @var EmailAttachment $item */
					$carry .= $item->getFilename();
					return $carry;
				},
				'');
		}

		if ($this->flags & self::USE_ATTACHMENT_MIMETYPE) {
			$contentToHash .= array_reduce(
				$email->getAttachments(),
				static function ($carry, $item) {
					/** @var EmailAttachment $item */
					$carry .= $item->getContentType();
					return $carry;
				},
				'');
		}

		if ($this->flags & self::USE_BODY_HTML) {
			$contentToHash .= $email->getHtmlContent();
		}

		if ($this->flags & self::USE_BODY_TEXT) {
			$contentToHash .= $email->getTextContent();
		}

		if ($this->flags & self::USE_CC_EMAIL) {
			$contentToHash .= array_reduce(
				$email->getCcAddresses(),
				static function ($carry, $item) {
					/** @var EmailAddress $item */
					$carry .= $item->getEmail();
					return $carry;
				},
				'');
		}

		if ($this->flags & self::USE_CC_NAME) {
			$contentToHash .= array_reduce(
				$email->getCcAddresses(),
				static function ($carry, $item) {
					/** @var EmailAddress $item */
					$carry .= $item->getName();
					return $carry;
				},
				'');
		}

		if ($this->flags & self::USE_DATE) {
			$contentToHash .= $email->getDate()->format('Y-m-d H:i:s');
		}

		if ($this->flags & self::USE_FROM_EMAIL) {
			$contentToHash .= $email->getFromEmail();
		}

		if ($this->flags & self::USE_FROM_NAME) {
			$contentToHash .= $email->getFromName();
		}

		if ($this->flags & self::USE_TO_EMAIL) {
			$contentToHash .= array_reduce(
				$email->getToAddresses(),
				static function ($carry, $item) {
					/** @var EmailAddress $item */
					$carry .= $item->getEmail();
					return $carry;
				},
				'');
		}

		if ($this->flags & self::USE_TO_NAME) {
			$contentToHash .= array_reduce(
				$email->getToAddresses(),
				static function ($carry, $item) {
					/** @var EmailAddress $item */
					$carry .= $item->getName();
					return $carry;
				},
				'');
		}

		return hash($hashingAlgorithm, $contentToHash);
	}
}
