<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use DateTime;
use ZBateson\MailMimeParser\Header\AddressHeader;
use ZBateson\MailMimeParser\Header\DateHeader;
use ZBateson\MailMimeParser\Message;

class EmailAdapter extends Email
{
	/** @var Message */
	protected $message;

	public function __construct(Message $message)
	{
		$this->message = $message;

		/** @var EmailAttachment[] $attachments */
		$attachments = [];

		foreach ($this->message->getAllAttachmentParts() ?? [] as $messagePart) {
			$attachment = new EmailAttachment();
			$attachment->setContent($messagePart->getContent());
			$attachment->setContentType($messagePart->getContentType());
			$attachment->setFilename($messagePart->getFilename());
			$attachments[] = $attachment;
		}

		$this->setAttachments($attachments);

		$this->setHtmlContent($message->getHtmlContent());

		$this->setTextContent($message->getTextContent());

		$this->setCcAddresses($this->getAddressesForHeader('Cc'));

		$this->setToAddresses($this->getAddressesForHeader('To'));

		$this->setDate($this->getDateFromHeader());

		/** @var AddressHeader $emailHeader */
		$emailHeader = $message->getHeader('From');

		if (null !== $emailHeader) {
			$this->setFromEmail($emailHeader->getEmail());
			$this->setFromName($emailHeader->getPersonName());
		}
	}

	/**
	 * @param string $header
	 * @return EmailAddress[] If header isn't found returns empty array
	 */
	private function getAddressesForHeader(string $header): array
	{
		/** @var EmailAddress[] $addresses */
		$addresses = [];

		/** @var AddressHeader $addressHeader */
		if (null === $addressHeader = $this->message->getHeader($header)) {
			return $addresses;
		}

		foreach ($addressHeader->getAddresses() as $item) {
			$address = new EmailAddress();
			$address->setEmail($item->getEmail());
			$address->setName($item->getName());
			$addresses[] = $address;
		}

		return $addresses;
	}

	private function getDateFromHeader(): DateTime
	{
		$date = new DateTime('0000-00-00');

		/** @var DateHeader $dateHeader */
		if (null === $dateHeader = $this->message->getHeader('date')) {
			return $date;
		}

		return $dateHeader->getDateTime() ?: $date;
	}
}
