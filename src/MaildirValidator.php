<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

class MaildirValidator
{
	/** @var Maildir */
	private $maildir;

	public function __construct(Maildir $maildir)
	{
		$this->maildir = $maildir;
	}

	public function validate(): void
	{
		if (! file_exists($this->maildir->getPath())) {
			throw new MaildirValidationException('',MaildirValidationException::MAILDIR_DOES_NOT_EXIST);
		}

		$this->validateMaildirStructure($this->maildir->getPath());
		foreach ($this->maildir->getSubDirs() as $dir) {
			$this->validateMaildirStructure($dir);
		}
	}

	private function validateMaildirStructure(string $path): void
	{
		foreach (Maildir::DIR_STRUCTURE as $dir) {
			if (! file_exists($path . $dir)) {
				throw new MaildirValidationException("Directory $dir doesn't exist.",MaildirValidationException::MAILDIR_HAS_NOT_VALID_FORMAT);
			}
		}
	}

}
