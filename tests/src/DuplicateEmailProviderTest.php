<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ZBateson\MailMimeParser\MailMimeParser;

/**
 * @coversDefaultClass \DuplicateEmailFinder\DuplicateEmailProvider
 */
final class DuplicateEmailProviderTest extends TestCase
{
	/** @var Maildir|MockObject */
	protected $maildir;

	/** @var EmailHashCache|MockObject */
	protected $emailHashCache;

	/** @var EmailHashGenerator|MockObject */
	protected $emailHashGenerator;

	/** @var MailMimeParser|MockObject */
	protected $mailMimeParser;

	protected function setUp(): void
	{
		$this->maildir = $this->createMock(Maildir::class);
		$this->emailHashCache = $this->createMock(EmailHashCache::class);
		$this->emailHashGenerator = $this->createMock(EmailHashGenerator::class);
		$this->mailMimeParser = $this->createMock(MailMimeParser::class);

		parent::setUp();
	}

	public function testCanReturnDuplicateEmails(): void
	{
		$this->emailHashCache
			->method('get')
			->willReturn([
				0 => [
					'hash' => 'same_hash',
					'email_path' => 'email_path_1',
				],
				1 => [
					'hash' => 'same_hash',
					'email_path' => 'email_path_2',
				],
				2 => [
					'hash' => 'other_hash',
					'email_path' => 'email_path_3',
				],
			]);

		$this->emailHashCache
			->expects(self::never())
			->method('flush');

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$result = $duplicateEmailProvider->get(false);

		self::assertCount(1, $result);

		self::assertEquals(['email_path_1', 'email_path_2'], $result['same_hash']);
	}

	public function testCanUseCache(): void
	{
		$this->emailHashCache
			->expects(self::once())
			->method('flush');

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$duplicateEmailProvider->get();
	}

	public function testCanExcludeFirstEmails(): void
	{
		$cache = require __DIR__ . '/../fixtures/emailHashCache.php';

		$this->emailHashCache
			->method('get')
			->willReturn($cache);

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$result = $duplicateEmailProvider->excludeFirst()->get(false);

		self::assertEquals(
			[
				'0fe3ae0240843b462721d6f88986ee690fbfb895' =>
					['tests/fixtures/Maildir/.Personal/cur/1601332792.M790307P28397.Somehost,S=6682,W=6777:2,S'],
				'43d19d1f2b1125c29cc80c4827dd4b160b001d97' =>
					['tests/fixtures/Maildir/.Personal/cur/1601332792.M661090P28397.Somehost,S=4334,W=4401:2,S'],
			],
			$result);
	}

	public function testCanExcludeLastEmails(): void
	{
		$cache = require __DIR__ . '/../fixtures/emailHashCache.php';

		$this->emailHashCache
			->method('get')
			->willReturn($cache);

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$result = $duplicateEmailProvider->excludeLast()->get(false);

		self::assertEquals(
			[
				'0fe3ae0240843b462721d6f88986ee690fbfb895' =>
					['tests/fixtures/Maildir/cur/1595055975.M865942P7056Q177Rf0f80106cc92b18d.Somehost:2,'],
				'43d19d1f2b1125c29cc80c4827dd4b160b001d97' =>
					['tests/fixtures/Maildir/cur/1595055976.M107259P7057Q178R8243f92e85840b74.Somehost:2,'],
			],
			$result);
	}

	public function testCanGetOnlyFiles(): void
	{
		$cache = require __DIR__ . '/../fixtures/emailHashCache.php';

		$this->emailHashCache
			->method('get')
			->willReturn($cache);

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$result = $duplicateEmailProvider->onlyFiles()->get(false);

		self::assertEquals(
			[
				'tests/fixtures/Maildir/cur/1595055975.M865942P7056Q177Rf0f80106cc92b18d.Somehost:2,',
				'tests/fixtures/Maildir/.Personal/cur/1601332792.M790307P28397.Somehost,S=6682,W=6777:2,S',
				'tests/fixtures/Maildir/cur/1595055976.M107259P7057Q178R8243f92e85840b74.Somehost:2,',
				'tests/fixtures/Maildir/.Personal/cur/1601332792.M661090P28397.Somehost,S=4334,W=4401:2,S',
			],
			$result);
	}

	public function testThrowsExceptionIfEmailCouldNotBeRead(): void
	{
		$this->maildir
			->method('getEmailPaths')
			->willReturn(['unknown_email_path']);

		$duplicateEmailProvider = new DuplicateEmailProvider(
			$this->maildir,
			$this->emailHashCache,
			$this->emailHashGenerator,
			$this->mailMimeParser
		);

		$this->expectException(RuntimeException::class);

		$duplicateEmailProvider->get();
	}
}
