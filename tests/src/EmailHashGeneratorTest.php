<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use PHPUnit\Framework\TestCase;
use ZBateson\MailMimeParser\MailMimeParser;

/**
 * @coversDefaultClass \DuplicateEmailFinder\EmailHashGenerator
 */
final class EmailHashGeneratorTest extends TestCase
{
	public function testCanGenerateAHashFromEmail(): void
	{
		$emailHashGenerator = new EmailHashGenerator(
			EmailHashGenerator::USE_ATTACHMENT_CONTENT
			| EmailHashGenerator::USE_ATTACHMENT_FILENAME
			| EmailHashGenerator::USE_ATTACHMENT_MIMETYPE
			| EmailHashGenerator::USE_BODY_HTML
			| EmailHashGenerator::USE_BODY_TEXT
			| EmailHashGenerator::USE_CC_EMAIL
			| EmailHashGenerator::USE_CC_NAME
			| EmailHashGenerator::USE_DATE
			| EmailHashGenerator::USE_FROM_EMAIL
			| EmailHashGenerator::USE_FROM_NAME
			| EmailHashGenerator::USE_TO_EMAIL
			| EmailHashGenerator::USE_TO_NAME
		);

		$email =  new EmailAdapter((new MailMimeParser())->parse(file_get_contents(__DIR__ . '/../fixtures/Maildir/cur/1595055975.M865942P7056Q177Rf0f80106cc92b18d.Somehost:2,')));

		$hash = $emailHashGenerator->generate($email);

		self::assertNotEmpty($hash);

		echo "Hash: $hash\n";
	}
}
