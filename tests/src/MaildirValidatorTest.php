<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \DuplicateEmailFinder\MaildirValidator
 */
final class MaildirValidatorTest extends TestCase
{
	public function testValidateThrowsExceptionWhenPathDoesNotExist(): void
	{
		$maildir = $this->createMock(Maildir::class);
		$maildir
			->method('getPath')
			->willReturn('some_not_existing_path');

		$this->expectException(MaildirValidationException::class);
		$this->expectExceptionCode(MaildirValidationException::MAILDIR_DOES_NOT_EXIST);

		$maildirValidator = new MaildirValidator($maildir);

		$maildirValidator->validate();
	}

	public function testValidateThrowsExceptionWhenMaildirIsNotValid(): void
	{
		$maildir = new Maildir(__DIR__ . '/../fixtures/Invalid_Maildir');

		$this->expectException(MaildirValidationException::class);
		$this->expectExceptionCode(MaildirValidationException::MAILDIR_HAS_NOT_VALID_FORMAT);

		$maildirValidator = new MaildirValidator($maildir);

		$maildirValidator->validate();
	}

	public function testValidateThrowsExceptionWhenMaildirSubdirectoryIsNotValid(): void
	{
		$maildir = new Maildir(__DIR__ . '/../fixtures/Invalid_Maildir_Subdirectory');

		$this->expectException(MaildirValidationException::class);
		$this->expectExceptionCode(MaildirValidationException::MAILDIR_HAS_NOT_VALID_FORMAT);

		$maildirValidator = new MaildirValidator($maildir);

		$maildirValidator->validate();
	}
}
