<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use DateTime;
use PHPUnit\Framework\TestCase;
use ZBateson\MailMimeParser\Header\AddressHeader;
use ZBateson\MailMimeParser\Header\DateHeader;
use ZBateson\MailMimeParser\Header\Part\AddressPart;
use ZBateson\MailMimeParser\Message;
use ZBateson\MailMimeParser\Message\Part\MessagePart;

/**
 * @coversDefaultClass \DuplicateEmailFinder\EmailAdapter
 */
final class EmailAdapterTest extends TestCase
{
	public function testCanAdaptAnEmail(): void
	{
		$message = $this->createMock(Message::class);

		$attachment1 = $this->createMock(MessagePart::class);

		$attachment1
			->method('getContent')
			->willReturn('123');
		$attachment1
			->method('getContentType')
			->willReturn('text/plain');
		$attachment1
			->method('getFilename')
			->willReturn('test_file.txt');

		$attachment2 = $this->createMock(MessagePart::class);

		$attachment2
			->method('getContent')
			->willReturn('123');
		$attachment2
			->method('getContentType')
			->willReturn('text/plain');
		$attachment2
			->method('getFilename')
			->willReturn('test_file.txt');

		$message
			->method('getAllAttachmentParts')
			->willReturn([
				$attachment1,
				$attachment2
			]);

		$message
			->method('getHtmlContent')
			->willReturn('<strong>Content</strong>');
		$message
			->method('getTextContent')
			->willReturn('Content');

		$ccAddressHeader = $this->createMock(AddressHeader::class);

		$ccAddress = $this->createMock(AddressPart::class);
		$ccAddress
			->method('getEmail')
			->willReturn('alex.doe@test');
		$ccAddress
			->method('getName')
			->willReturn('Alex Doe');

		$ccAddressHeader
			->method('getAddresses')
			->willReturn([$ccAddress]);

		$toAddressHeader = $this->createMock(AddressHeader::class);

		$toAddress = $this->createMock(AddressPart::class);
		$toAddress
			->method('getEmail')
			->willReturn('john.doe@test');
		$toAddress
			->method('getName')
			->willReturn('John Doe');

		$toAddressHeader
			->method('getAddresses')
			->willReturn([$toAddress]);

		$dateHeader = $this->createMock(DateHeader::class);

		$dateHeader
			->method('getDateTime')
			->willReturn(new DateTime());

		$emailHeader = $this->createMock(AddressHeader::class);
		$emailHeader
			->method('getEmail')
			->willReturn('zoe.deo@test');
		$emailHeader
			->method('getPersonName')
			->willReturn('Zoe Doe');

		$message
			->method('getHeader')
			->willReturnOnConsecutiveCalls(
				$ccAddressHeader,
				$toAddressHeader,
				$dateHeader,
				$emailHeader
			);


		$email = new EmailAdapter($message);

		self::assertCount(2, $email->getAttachments());
		self::assertEquals('123', $email->getAttachments()[0]->getContent());
		self::assertEquals('text/plain', $email->getAttachments()[0]->getContentType());
		self::assertEquals('test_file.txt', $email->getAttachments()[0]->getFilename());

		self::assertEquals('<strong>Content</strong>', $email->getHtmlContent());
		self::assertEquals('Content', $email->getTextContent());

		self::assertCount(1, $email->getCcAddresses());
		self::assertEquals('alex.doe@test', $email->getCcAddresses()[0]->getEmail());
		self::assertEquals('Alex Doe', $email->getCcAddresses()[0]->getName());

		self::assertCount(1, $email->getToAddresses());
		self::assertEquals('john.doe@test', $email->getToAddresses()[0]->getEmail());
		self::assertEquals('John Doe', $email->getToAddresses()[0]->getName());

		self::assertNotEquals(new DateTime('0000-00-00'), $email->getDate());

		self::assertEquals('zoe.deo@test', $email->getFromEmail());
		self::assertEquals('Zoe Doe', $email->getFromName());

	}

	public function testCanAdaptAnEmailWithoutDate(): void
	{
		$message = $this->createMock(Message::class);

		$email = new EmailAdapter($message);

		self::assertEquals(new DateTime('0000-00-00'), $email->getDate());
	}
}
