<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \DuplicateEmailFinder\Maildir
 */
final class MaildirTest extends TestCase
{
	public function testCanCreateAMaildir(): void
	{
		$maildir = new Maildir('test');

		self::assertSame('test', $maildir->getPath());
	}

	public function testCanReturnSubdirectories(): void
	{
		$maildir = new Maildir(__DIR__ . '/../fixtures/Maildir');
		self::assertContains(__DIR__ . '/../fixtures/Maildir/.Personal', $maildir->getSubDirs());
	}

	public function testCanGetEmailPaths(): void
	{
		$maildir = new Maildir(__DIR__ . '/../fixtures/Maildir');
		$emailPaths = $maildir->getEmailPaths();
		self::assertCount(4, $emailPaths);
		self::assertContains(__DIR__ . '/../fixtures/Maildir/.Personal/cur/1601332792.M661090P28397.Somehost,S=4334,W=4401:2,S', $emailPaths);
		self::assertContains(__DIR__ . '/../fixtures/Maildir/.Personal/cur/1601332792.M790307P28397.Somehost,S=6682,W=6777:2,S', $emailPaths);
		self::assertContains(__DIR__ . '/../fixtures/Maildir/cur/1595055975.M865942P7056Q177Rf0f80106cc92b18d.Somehost:2,', $emailPaths);
		self::assertContains(__DIR__ . '/../fixtures/Maildir/cur/1595055976.M107259P7057Q178R8243f92e85840b74.Somehost:2,', $emailPaths);
	}
}
