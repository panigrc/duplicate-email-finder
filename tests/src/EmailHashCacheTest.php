<?php

declare(strict_types=1);

/*
 * This file is part of DuplicateEmailFinder.
 *
 * (c) Nikolaos Papagiannopoulos <panigrc@x-park.gr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DuplicateEmailFinder;

use PHPUnit\Framework\TestCase;
use \RuntimeException;

/**
 * @coversDefaultClass \DuplicateEmailFinder\EmailHashCache
 */
final class EmailHashCacheTest extends TestCase
{
	private $cache;

	private $cacheFilePath = '/tmp/emailHashCache.php';

	public function setUp(): void
	{
		$this->cache = new EmailHashCache($this->cacheFilePath);
	}

	public function tearDown(): void
	{
		$this->cache->clear();
	}

	public function testCanUseCache(): void
	{
		$data = [];

		$data[] = ['hash' => sha1('test'), 'email_path' => 'test'];

		$this->cache->append($data[0]['hash'], $data[0]['email_path']);

		self::assertFileDoesNotExist($this->cacheFilePath);

		$this->cache->flush();

		self::assertFileExists($this->cacheFilePath);

		self::assertEquals($data, $this->cache->get());
	}

	public function testThrowsExceptionWhenReadingIfCacheFileDoesNotExist(): void
	{
		$this->expectException(RuntimeException::class);

		$cache = new EmailHashCache('unknownFilePath.php');

		$cache->read();

		self::assertTrue(true);
	}

	public function testThrowsExceptionWhenWritingIfCacheIsEmpty(): void
	{
		$this->expectException(RuntimeException::class);

		$this->cache->flush();
	}

	public function testThrowsExceptionWhenWritingIfDirectoryDoesNotExist(): void
	{
		$this->expectException(RuntimeException::class);

		$cache = new EmailHashCache('/tmp/not/existing/directory/emailHashCache.php');

		$cache->append('hash', 'emailPath', true);
	}
}
